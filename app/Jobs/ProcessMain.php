<?php

namespace App\Jobs;

use App\Workers\ImportedProfilesWorker;
use Exception;
use App\Account;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessMain implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $account;

    /**
     * ProcessMain constructor.
     * @param Account $account
     */
    public function __construct(Account $account)
    {
        $this->account = $account;

        $account->status = Account::STATUS_ON_PROCESS;

        $account->save();

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info(' start handle '. $this->account->username);

        sleep(3);

        new ImportedProfilesWorker($this->account);

        dump($this->account->username);

        $this->account->status = Account::STATUS_FREE;

        $this->account->save();
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::error('exeption in failed method');
        // Send user notification of failure, etc...
//        sleep(10);
    }


    public function checkProxy()
    {
        // check proxy
    }
}
