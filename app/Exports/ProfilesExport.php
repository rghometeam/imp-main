<?php

namespace App\Exports;

use App\Profile;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;

class ProfilesExport implements  FromQuery, WithMapping, WithHeadings
{
    use Exportable;


    public function __construct($date)
    {
        $this->date = $date;
    }

    public function query()
    {
        return Profile::query()->whereIn('status', [ Profile::STATUS_CHECK, Profile::STATUS_CHECK_WITH_FOLLOWERS ])
            ->where('univer_id', '=', null);
    }

    /**
     * @var Profile $profile
     */
    public function map($profile): array
    {
        return [
            $profile->username,
            $profile->is_private,
            $profile->like_average,
            $profile->followers,
            $profile->average_engagement,
            $profile->comment_average_engagement,
            $profile->comment_average,
            $profile->public_email,
            $profile->parent_id,
            $profile->created_at,
        ];
    }

    public function headings(): array
    {
        return [
            'USERNAME',
            'IS PRIVATE',
            'LIKE AVERAGE',
            'FOLLOWERS',
            'LIKES AVERAGE ENGAGEMENT',
            'COMMENTS AVERAGE ENGAGEMENT',
            'COMMENTS AVERAGE',
            'EMAIL',
            'PARENT ID',
            'CREATED DATE',
        ];
    }
}
