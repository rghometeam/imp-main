<?php

namespace App\Exports;

use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class UniverExport implements WithMultipleSheets, Responsable
{
    use Exportable;

    protected $reports;
    protected $summary;

    public function __construct()
    {
//        $this->summary = $summary;
//        $this->reports = $reports;
    }

    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new UniverProfilesPlaceExport();
        $sheets[] = new UniverProfilesInstaPageExport();

        return $sheets;
    }
}
