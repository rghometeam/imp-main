<?php

namespace App\Imports;

use App\Profile;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Validators\Failure;

class ProfilesImport implements ToModel, WithValidation,  SkipsOnFailure
{

    use Importable, SkipsFailures;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Profile([
            'username' => $this->formattedName($row[0]),
        ]);
    }

    /**
     * @param $username
     * @return bool|string
     */
    public function formattedName($username)
    {
        if ($username[0] === '@') {
            $username = substr($username, 1);
        }

        return $username;
    }

    /**
     * @param Failure[] $failures
     */
    public function onFailure(Failure ...$failures)
    {
        // Handle the failures how you'd like.
    }

    public function rules(): array
    {

        return [
            '0' => 'required|unique:profiles,username',
        ];
    }
}
