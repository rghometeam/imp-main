<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proxy extends Model
{
    const STATUS_FREE = 0;
    const STATUS_ON_PROCESS = 1;
    const STATUS_FAILED = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ip', 'status'
    ];
}
