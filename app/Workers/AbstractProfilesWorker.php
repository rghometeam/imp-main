<?php
/**
 *
 */

namespace App\Workers;


use App\Account;
use App\Profile;
use App\Services\InstagramProvider;
use App\Univer;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

abstract class AbstractProfilesWorker
{
    /**
     * @var InstagramProvider $instagram
     */
    protected $instagram;

    /**
     * @var Account $account
     */
    protected $account;

    public function __construct($account)
    {
        $this->account = $account;

        $this->instagram = new InstagramProvider($this->account);

    }

    abstract public function handle();

    /**
     * @param Profile $profile
     * @throws \Exception
     */
    public function loadFollowers($profile)
    {
        try {
            $this->saveFollowersBy($profile);
            $profile->status = Profile::STATUS_CHECK_WITH_FOLLOWERS;
            $profile->save();
        } catch (\Exception $exception) {
            dump($exception->getMessage());
            if ($exception->getMessage() == 'Throttled by Instagram because of too many API requests.') {
                $profile->status = Profile::STATUS_CHECK;
                $profile->save();
                sleep(4);
            }
            if ($exception->getMessage() == 'User not logged in. Please call login() and then try again.') {
                throw $exception;
            }
            return;
        }
    }

    /**
     * @param Profile $profile
     * @throws \Exception
     */
    public function checkProfile($profile)
    {
        try {
            $this->saveResultFrom($profile);
        } catch (\Exception $exception) {
            if ($exception->getMessage() == 'Throttled by Instagram because of too many API requests.') {
                $profile->status = Profile::STATUS_NEW;
                $profile->save();
                sleep(rand(5,9));
                return;
            }
            if ($exception->getMessage() == 'InstagramAPI\Response\UserInfoResponse: User not found.') {
                $profile->status = Profile::STATUS_USER_NOT_FOUND;
                $profile->save();
                return;
            }
            if ($exception->getMessage() == 'User not logged in. Please call login() and then try again.') {
                throw $exception;
            }

            dump(['exception' => $exception->getMessage(), 'profile' => $profile->username]);

            $profile->status = Profile::STATUS_UNCHECK;
            $profile->save();

            return;
        }
    }

    /**
     * @param bool $isParent
     * @return Profile
     */
    public function getUncheckProfiles(bool $isParent)
    {
        if ($isParent) {
            /**
             * @var Profile $profile
             */
            $profile = Profile::where('status', Profile::STATUS_NEW)->where('parent_id', null)
                ->where('univer_id', null)
                ->first();
        } else {
            /**
             * @var Profile $profile
             */
            $profile = Profile::where('status', Profile::STATUS_NEW)->where('parent_id', '!=', null)
                ->where('univer_id', null)
                ->first();
        }


        if (isset($profile)) {
            $profile->status = Profile::STATUS_ON_PROCESS;

            $profile->save();
        }

        return $profile;
    }

    /**
     * @return Profile
     */
    public function getCheckProfiles()
    {
        /**
         * @var Profile $profile
         */
        $profile = Profile::where('status', Profile::STATUS_CHECK)->where('parent_id', null)->first();

        if (isset($profile)) {
            $profile->status = Profile::STATUS_ON_PROCESS;

            $profile->save();
        }

        return $profile;
    }

    /**
     * @param Profile $profile
     * @param bool $isStudent
     */
    public function saveResultFrom(Profile $profile, $isStudent = false)
    {
//        sleep(1);
        $user = $this->instagram->getInfo($profile->username);

        if (empty($user) || !isset($user)) {
            $profile->status = Profile::STATUS_UNCHECK;
            $profile->save();
            return;
        }

        if ($user->isIsPrivate()) {
            $profile->is_private = 1;
        } else {

            $profile->is_private = 0;

            $profile->followers = $user->getFollowerCount();
            $profile->public_email = $user->getPublicEmail();

            //todo if return $likeCount=0 doing something
            $likeCount = $this->getLikesFromPosts($profile->username, 10);

            if ($likeCount !== 0) {
                $profile->average_engagement = (float) ( $likeCount / $user->getFollowerCount());
                $profile->like_average = (float) ($likeCount / 10);
            }

            if ($isStudent) {
                $profile->following = $user->getFollowingCount();
            } else {
                $commentCount = $this->getCommentsFromPosts($profile->username, 10);
                dump('CommentsFromPosts = '. $commentCount);
                if ($commentCount !== 0) {
                    $profile->comment_average_engagement = (float) ( $commentCount / $user->getFollowerCount());
                    $profile->comment_average = (float) ($commentCount / 10);
                }
            }
        }

        $profile->status = Profile::STATUS_CHECK;

        $profile->save();
    }

    /**
     * @param Profile $profile
     */
    public function saveFollowersBy($profile)
    {
        if ($profile->parent_id == null) {
            sleep(1);
            $followers = $this->instagram->getFolowers($profile->username, $this->account->max_followers);

            if (count($followers) > 0) {

                foreach ($followers as $follower) {
                    try {
                        $this->createProfile(
                            [
                            'username' => $follower['username'],
                            'is_private' => (int)$follower['is_private'],
                            'parent_id' => $profile->id
                            ]
                        );
                    } catch (\Exception $exception) {
                    }
                }
                dump(['account' => $this->account->username, 'followerAdded' => count($followers)]);
            }
        }
    }

    /**
     * @param $username
     * @param $maxPosts
     * @return int
     */
    public function getLikesFromPosts($username, $maxPosts)
    {
//        sleep(1);
        $likes = 0;
        $result = $this->instagram->getUserPosts($username)->asArray();

        if ($result['items']) {
            foreach ($result['items'] as $index => $post) {
                if ($index + 1 == $maxPosts) return $likes;

                $likes += $post['like_count'];
            }
        }

        return $likes;
    }

    /**
     * @param $username
     * @param $maxPosts
     * @return int
     */
    public function getCommentsFromPosts($username, $maxPosts)
    {
//        sleep(1);
        $comments = 0;
        $result = $this->instagram->getUserPosts($username);

        if ($result->getItems()) {

            foreach ($result->getItems() as $index => $post) {
                if ($index + 1 == $maxPosts) return $comments;
                $commentsFromPostCount = $this->instagram->getMediaCommentsCount($post->getId());

                $comments += $commentsFromPostCount;
            }
        }

        return $comments;
    }

    /**
     * @param array $data
     * @return Profile
     */
    public function createProfile(array $data)
    {
        return Profile::create($data);
    }

}