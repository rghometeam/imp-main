<?php
/**
 *
 */

namespace App\Workers;


use App\Account;
use App\Profile;
use App\Services\InstagramProvider;
use App\Univer;
use Illuminate\Support\Facades\Log;

class UniverWorker extends AbstractProfilesWorker
{
    /**
     * @var InstagramProvider $instagram
     */
    protected $instagram;

    /**
     * @var Account $account
     */
    protected $account;

    public function __construct($account)
    {
        parent::__construct($account);

        $this->handle();
    }


    public function handle()
    {
        $this->univerCheckerLoop();
    }

    public function univerCheckerLoop()
    {
        /**
         * @var Univer $univer
         */
        $univer = $this->getUncheckUniver();

        while (isset($univer)) {
            $this->grabStudentProfilesFrom($univer, true);
            $this->grabStudentProfilesFrom($univer);
            $univer->status = Univer::STATUS_CHECK;
            $univer->save();
            $univer = $this->getUncheckUniver();
        }
    }

    /**
     * @param Univer $univer
     * @param $isPlace
     */
    public function grabStudentProfilesFrom($univer, $isPlace = false)
    {
        if ($isPlace) {
            $query = $this->getLocationQuery($univer);

            //todo delete limit
            $posts = $this->instagram->getPlaceMedia($query, Univer::MAX_POSTS_COUNT);
        } else {
            $posts = $this->instagram->getProfileUniverMedia($univer->name, Univer::MAX_POSTS_COUNT);
        }


        try {
            foreach ($posts as $post) {
                sleep(1);
                $mediaId = $isPlace ? $post['header_media']['media'][0]['id'] : $post['id'];

                if ($isPlace) {
                    try {
                        $username = $post['header_media']['media'][0]['user']['username'];

                        $this->saveUniverProfile($username, $univer, $mediaId, true);
                    dump(['PostUser' => $username, 'univer' => $univer->name, 'media' => $mediaId]);
//                        fwrite(fopen('/tmp/dump', 'a'), print_r(
//                            ['PostUser' => $username, 'univer' => $univer->name, 'media' => $mediaId], 1));
                    } catch (\Exception $exception) {
                        Log::error($exception->getMessage());
                    }
                }


                sleep(1);
                $this->prepareLikers($mediaId, $univer, $isPlace);

                sleep(1);
                $this->prepareComments($mediaId, $univer, $isPlace);



            }
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }
    }

    /**
     * @param $username
     * @param $univer
     * @param $mediaId
     * @param bool $isPlace
     */
    public function saveUniverProfile($username, $univer, $mediaId, $isPlace = false)
    {
        try {
            if (isset($username) && isset($univer)) {
                if ($isPlace) {
                    $profile = $this->createProfile(
                        [
                            'username' => $username,
                            'univer_id' => $univer->id,
                            'media_id' => $mediaId
                        ]
                    );
                } else {
                    $profile = $this->createProfile(
                        [
                            'username' => $username,
                            'univer_id' => $univer->id
                        ]
                    );
                }

                $this->saveResultFrom($profile, true);
            }
        } catch (\Exception $exception) {
            if ($exception->getMessage() == 'Throttled by Instagram because of too many API requests.') {
                sleep(rand(8,12));
                $profile = Profile::where('username', $username)->first();
                $this->saveResultFrom($profile, true);
            }
        }
    }

    /**
     * @return Univer
     */
    public function getUncheckUniver()
    {

        /**
         * @var Univer $univer
         */
        $univer = Univer::where('status', Profile::STATUS_NEW)->first();


        if (isset($univer)) {
            $univer->status = Univer::STATUS_ON_PROCESS;

            $univer->save();
        }

        return $univer;
    }

    /**
     * @param $univer
     * @return string
     */
    public function getLocationQuery($univer)
    {
        return $this->instagram->getInfo($univer->name)->getFullName();
    }

    /**
     * @param $mediaId
     * @param Univer $univer
     * @param bool $isPlace
     */
    public function prepareComments($mediaId, Univer $univer, bool $isPlace)
    {
        $comments = $this->instagram->getMediaComments($mediaId, Univer::MAX_LIKERS_COMMENTS_COUNT);

        foreach ($comments as $index => $comment) {
            //todo delete this limit
            if ($index > Univer::MAX_LIKERS_COMMENTS_COUNT) break;
            try {
                $username = $comment['user']['username'];
                // if this profile from place set media_id
                // if not this profile from univer page
                if ($isPlace) {
                    $this->saveUniverProfile($username, $univer, $mediaId, true);
                } else {
                    $this->saveUniverProfile($username, $univer, $mediaId);
                }
                        dump(['CommentUser' => $username, 'univer' => $univer->name, 'media' => $mediaId]);
//                fwrite(fopen('/tmp/dump', 'a'), print_r(
//                    ['CommentUser' => $username, 'univer' => $univer->name, 'media' => $mediaId], 1));
            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
            }
        }
    }

    /**
     * @param $mediaId
     * @param Univer $univer
     * @param bool $isPlace
     */
    public function prepareLikers($mediaId, Univer $univer, bool $isPlace)
    {
        $likers = $this->instagram->getMediaLikers($mediaId);

        if (isset($likers['users'])) {
            foreach ($likers['users'] as $index => $liker) {
                //todo delete this limit
                if ($index > Univer::MAX_LIKERS_COMMENTS_COUNT) break;
                try {
                    $username = $liker['username'];
                    // if this profile from place set media_id
                    // if not this profile from univer page
                    if ($isPlace) {
                        $this->saveUniverProfile($username, $univer, $mediaId, true);
                    } else {
                        $this->saveUniverProfile($username, $univer, $mediaId);
                    }
                            dump(['LikeUser' => $username, 'univer' => $univer->name, 'media' => $mediaId]);
//                    fwrite(fopen('/tmp/dump', 'a'), print_r(
//                        ['LikeUser' => $username, 'univer' => $univer->name, 'media' => $mediaId], 1));
                } catch (\Exception $exception) {
                    Log::error($exception->getMessage());
                }
            }
        }
    }
}