<?php
/**
 *
 */

namespace App\Workers;


use App\Account;
use App\Profile;
use App\Services\InstagramProvider;

class ImportedProfilesWorker extends AbstractProfilesWorker
{
    /**
     * @var InstagramProvider $instagram
     */
    protected $instagram;

    /**
     * @var Account $account
     */
    protected $account;

    public function __construct($account)
    {
        parent::__construct($account);

        $this->handle();
    }

    /**
     * main process
     */
    public function handle()
    {
        $this->mainCheckerLoop(true);

        $this->followerLoaderLoop();

        $this->mainCheckerLoop(false);

    }

    /**
     * @param bool $isParent
     */
    public function mainCheckerLoop(bool $isParent)
    {
        $count = 0;

        /**
         * @var Profile $profile
         */
        $profile = $this->getUncheckProfiles($isParent);

        while (isset($profile)) {
            if ($count == 20) {
                sleep(rand(2, 3));
                $count = 0;
            }

            $this->checkProfile($profile);

            dump(['profile' => $profile->username, 'account' => $this->account->username ]);

            $count++;

            $profile = $this->getUncheckProfiles($isParent);
        }
    }

    /**
     * @return mixed
     */
    public function followerLoaderLoop()
    {
        /**
         * @var Profile $profile
         */
        $profile = $this->getCheckProfiles();

        while (isset($profile)) {
            $this->loadFollowers($profile);
            $profile = $this->getCheckProfiles();
        }
    }
}