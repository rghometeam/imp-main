<?php

namespace App\Console\Commands;


use App\Imports\UniversitiesImport;
use App\Univer;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Console\Command;

class UniverImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ig:college:import {--path=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = $this->option('path');

        if (isset($path)) {
            Excel::import(new UniversitiesImport(), $path);


            $all = Univer::get()->count();

            $this->info('Colleges successfully imported');
            $this->info('Total colleges profiles: '. $all);

        }
    }
}
