<?php

namespace App\Console\Commands;


use App\Exports\ProfilesExport;
use App\Exports\UniverExport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class ExportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ig:export {--mode=} {--fileName=}, {--from=} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fileName = $this->option('fileName');

        $from = $this->option('from');

        $mode = $this->option('mode');

        if (isset($mode) && $mode == 'college') {
            $this->exportUniverProfiles($fileName);
        } else {
            $this->exportImportedProfiles($fileName, $from);
        }

    }

    /**
     * @param $fileName
     * @param $from
     */
    public function exportImportedProfiles($fileName, $from) {
        if (isset($fileName)) {
            $file = Excel::download(new ProfilesExport($from), $fileName, \Maatwebsite\Excel\Excel::XLSX);

            $file->getFile()->move(public_path(). '/export', $fileName.'.xlsx');

            $this->info('Profiles successfully export to ' . public_path(). '/export/'. $fileName.'.xlsx');
        }
    }

    /**
     * @param $fileName
     */
    public function exportUniverProfiles($fileName) {
        if (isset($fileName)) {
            $file = Excel::download(new UniverExport(), $fileName, \Maatwebsite\Excel\Excel::XLSX);

            $file->getFile()->move(public_path(). '/export', $fileName.'.xlsx');

            $this->info('Profiles successfully export to ' . public_path(). '/export/'. $fileName.'.xlsx');
        }
    }
}
