<?php

namespace App\Console\Commands;


use App\Account;
use InstagramAPI\Instagram;
use Illuminate\Console\Command;

class AddAccountsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ig:account:add {--login=} {--password=} {--maxFollowers=}' ;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $login = $this->option('login');
        $password = $this->option('password');
        $maxFollowers = $this->option('maxFollowers');

        $ig  = new Instagram(false,false,
            [
                'storage'    => env('DB_CONNECTION'),
                'dbhost'     => env('DB_HOST'),
                'dbname'     => env('DB_DATABASE'),
                'dbusername' => env('DB_USERNAME'),
                'dbpassword' => env('DB_PASSWORD'),
            ]);

        $loginResponse = $ig->login($login, $password);

        if (!is_null($loginResponse) && $loginResponse->isTwoFactorRequired()) {
            $twoFactorIdentifier = $loginResponse->getTwoFactorInfo()->getTwoFactorIdentifier();

            $code = $this->ask('Enter verification code:');
            $this->info('Login process...');

            $verificationCode = trim($code);
            $ig->finishTwoFactorLogin($login, $password, $twoFactorIdentifier, $verificationCode);

            if (isset($maxFollowers)) {
                Account::create(
                    [
                        'username' => $login,
                        'password' => $password,
                        'max_followers' => (int)$maxFollowers,
                        'status' => 0
                    ]);
            } else {
                Account::create(
                    [
                        'username' => $login,
                        'password' => $password,
                        'status' => 0
                    ]);
            }

            $this->info('Account successfully added');
        } else {
            if (isset($maxFollowers)) {
                Account::create(
                    [
                        'username' => $login,
                        'password' => $password,
                        'max_followers' => (int)$maxFollowers,
                        'status' => 0
                    ]);
            } else {
                Account::create(
                    [
                        'username' => $login,
                        'password' => $password,
                        'status' => 0
                    ]);
            }
            $this->info('Account successfully added');
        }

    }

}
