<?php

namespace App\Console\Commands;


use App\Exports\ProfilesExport;
use App\Profile;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class CleanCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ig:clear:profiles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Profile::truncate();
        $this->info('Profiles table is empty');
    }
}
