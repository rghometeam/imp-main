<?php

namespace App\Console\Commands;


use App\Imports\ProfilesImport;
use App\Profile;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Console\Command;

class ImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ig:import {--path=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $value = $this->option('path');

        if (isset($value)) {
            Excel::import(new ProfilesImport, $value);

            $countLastInsert = Profile::where('created_at', '>=', Carbon::now()->subSeconds(10)->toDateTimeString())->count();

            $all = Profile::get()->count();

            $this->info('Profiles successfully imported: ' . $countLastInsert);
            $this->info('Total profiles: '. $all);

        }
    }
}
