<?php

namespace App\Console\Commands;


use App\Account;
use App\Profile;
use fXmlRpc\Client;
use fXmlRpc\Transport\HttpAdapterTransport;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Supervisor\Connector\XmlRpc;
use Supervisor\Supervisor;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use \Http\Message\MessageFactory\DiactorosMessageFactory as MessageFactory;
use Symfony\Component\Process\Process;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class StopCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ig:stop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var Supervisor
     */
    private $supervisorInstance;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->supervisorConnection();

        $this->supervisorInstance->stopAllProcesses();

        try {
            $amqp = new AMQPStreamConnection(env('RABBITMQ_HOST'), env('RABBITMQ_NODE_HOST_PORT'),
                env('RABBITMQ_DEFAULT_USER'), env('RABBITMQ_DEFAULT_PASS'));

            $amqp->getConnection()->channel()->queue_delete('default');
        } catch (\Exception $exception) {
            dump($exception->getMessage());
            Log::error($exception->getMessage());
        }

        Account::where('status', 1)->update(['status' => 0]);

        Profile::where('status', Profile::STATUS_ON_PROCESS)->update(['status' => Profile::STATUS_NEW]);

        $this->info('Stopped');

        (new Process(['kill $(ps aux | grep \'[p]hp\' | awk \'{print $2}\')', '-9']))->run();
    }

    /**
     * Initialize supervisor RPC2 connection
     */
    protected function supervisorConnection()
    {
        //Create GuzzleHttp client
        $guzzleClient = new \GuzzleHttp\Client([
            'auth' => [
                env('SUPERVISOR_USER'),
                env('SUPERVISOR_PASS'),
            ]
        ]);

        $this->supervisorInstance = new Supervisor(
            new XmlRpc(
                new Client(
                    env('SUPERVISOR_RPC2_URL'),
                    new HttpAdapterTransport(
                        new MessageFactory(),
                        new GuzzleAdapter(
                            $guzzleClient
                        )
                    )
                )
            )
        );
    }
}
