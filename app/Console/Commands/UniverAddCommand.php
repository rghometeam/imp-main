<?php

namespace App\Console\Commands;


use App\Imports\UniversitiesImport;
use App\Univer;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Console\Command;

class UniverAddCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ig:college:add {--name=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->option('name');

        try {
            Univer::create(
                [
                    'name' => $name
                ])
            ;

            $this->info('College '. $name . ' was added');
        } catch (\Exception $exception) {
            $this->info('College '. $name . ' contain in table');
            Log::error($exception->getMessage());
        }

    }
}
