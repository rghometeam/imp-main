<?php

namespace App\Console\Commands;


use App\Imports\UniversitiesImport;
use App\Univer;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Console\Command;

class UniverRemoveCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ig:college:remove {--name=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->option('name');

        try {
            Univer::where('name', $name)->delete();

            $this->info('College '. $name . ' was removed');
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }

    }
}
