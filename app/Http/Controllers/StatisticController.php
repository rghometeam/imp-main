<?php

namespace App\Http\Controllers;



use App\Account;
use App\Profile;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Input;
use InstagramAPI\Instagram;
use Supervisor\Supervisor;
use Supervisor\Connector\XmlRpc;
use fXmlRpc\Client;
use fXmlRpc\Transport\HttpAdapterTransport;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use \Http\Message\MessageFactory\DiactorosMessageFactory as MessageFactory;


/**
 * Class StatisticController
 * @package App\Http\Controllers
 */
class StatisticController extends Controller
{

    /**
     * @var Supervisor|null
     */
    private $supervisorInstance;

    /**
     * StatisticController constructor.
     */
    public function __construct() {
        $this->supervisorConnection();
    }

    public function getStatistic()
    {
       $statistic = [];

       $statistic['workers'] = $this->supervisorInstance->getAllProcessInfo();

       $statistic['accounts'] = Account::all();

        $check = Profile::whereIn('status', [Profile::STATUS_CHECK, Profile::STATUS_CHECK_WITH_FOLLOWERS])
            ->count();

        $private = Profile::where('is_private', 1)
            ->count();

        $notFound = Profile::where('status', Profile::STATUS_USER_NOT_FOUND)
            ->count();

        $parentFollw = Profile::where('parent_id', '!=', null)
            ->count();

        $likeAverage = Profile::where('like_average', '!=', null)->count();

        $notPosts = Profile::where('like_average', null)
            ->where('status', Profile::STATUS_CHECK )
            ->where('is_private', 0)
            ->count();

        $notPosts += Profile::where('like_average', null)
            ->where('status', Profile::STATUS_CHECK_WITH_FOLLOWERS )
            ->where('is_private', 0)
            ->count();

        $total = Profile::count();

        $statistic['stat'] = ['checked' => $check, 'private' => $private, 'profiles not found' => $notFound,
                'received followers' => $parentFollw, 'profiles with likes' => $likeAverage,
                'haven\'t posts' => $notPosts, 'total' => $total];

       return new JsonResponse([ 'statistic' => $statistic ]);
    }

    /**
     * Initialize supervisor RPC2 connection
     */
    protected function supervisorConnection()
    {
        //Create GuzzleHttp client
        $guzzleClient = new \GuzzleHttp\Client([
            'auth' => [
                env('SUPERVISOR_USER'),
                env('SUPERVISOR_PASS'),
            ]
        ]);

        $this->supervisorInstance = new Supervisor(
            new XmlRpc(
                new Client(
                    env('SUPERVISOR_RPC2_URL'),
                    new HttpAdapterTransport(
                        new MessageFactory(),
                        new GuzzleAdapter(
                            $guzzleClient
                        )
                    )
                )
            )
        );
    }
}
