<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    const STATUS_FREE = 0;
    const STATUS_ON_PROCESS = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password', 'status', 'proxy', 'max_followers'
    ];
}
