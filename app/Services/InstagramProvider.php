<?php
/**
 *
 */

namespace App\Services;

use App\Account;
use App\Proxy;
use App\Univer;
use Illuminate\Support\Facades\Log;
use InstagramAPI\Instagram;


class InstagramProvider
{
    /**
     * @var Instagram $ig
     */
    private $ig;

    /**
     * @var Account $account
     */
    private $account;

    public function __construct($account)
    {
        $this->account = $account;

        $this->ig  = new Instagram(false,false,
            [
                'storage'    => env('DB_CONNECTION'),
                'dbhost'     => env('DB_HOST'),
                'dbname'     => env('DB_DATABASE'),
                'dbusername' => env('DB_USERNAME'),
                'dbpassword' => env('DB_PASSWORD'),
            ]);

        $this->login($this->account->username, $this->account->password);

    }


    public function getProfileUniverMedia($name, $max)
    {
        $userId = $this->getIdByUser($name);

        $medias = [];

        // Starting at "null" means starting at the first page.
        $maxId = null;

        do {

            //todo delete limit
            if (count($medias) >= $max) break;

            try {
                sleep(1);
                // Request the page corresponding to maxId.
                $response = $this->ig->timeline->getUserFeed($userId, $maxId);

                if ($response->isNextMaxId()) {
                    $maxId = $response->getNextMaxId();
                } else {
                    $maxId = null;
                }

                $items = $response->asArray()['items'];

                $medias = array_merge($medias,$items);
            } catch (\Exception $exception) {
                if ($exception->getMessage() == 'Throttled by Instagram because of too many API requests.') {
                    sleep(rand(7,12));
                } else {
                    throw $exception;
                }
            }

        } while ($maxId !== null);

        return $medias;
    }


    public function getPlaceMedia($place, $maxMedia)
    {
        $media = [];

        // Starting at "null" means starting at the first page.
        $rankToken = null;

        do {

            if (count($media) >= $maxMedia) break;

            try {
                sleep(1);
                // Request the page corresponding to maxId.
                $response = $this->ig->location->findPlaces($place, [], $rankToken) ;

                if ($response->isHasMore()) {
                    $rankToken = $response->getRankToken();
                }

                $items = $response->asArray()['items'];

                $media = array_merge($media,$items);
            } catch (\Exception $exception) {
                dump($exception->getMessage());
                if ($exception->getMessage() == 'Throttled by Instagram because of too many API requests.') {
                    sleep(rand(7,12));
                } else {
                    throw $exception;
                }
            }

        } while ($rankToken !== null);

//        if (count($followings) > $maxFollowers) {
//            $followings = array_slice($followings,0, $maxFollowers);
//        }

        return $media;
    }

    public function getMediaLikers($id)
    {
        $likers = $this->ig->media->getLikers($id);

        return $likers->asArray();
    }

    public function getMediaCommentsCount($id)
    {
        return $this->ig->media->getComments($id)->getCommentCount();
    }

    public function getMediaComments($id, $max = 0)
    {
        $comments = [];

        // Starting at "null" means starting at the first page.
        $maxId = null;

        do {

            //todo delete limit
            if ($max != 0 && count($comments) >= $max) break;

            try {
                sleep(1);
                // Request the page corresponding to maxId.
                $response = $this->ig->media->getComments($id,['max_id' => $maxId]);

                if ($response->isHasMoreComments()) {
                    $maxId = $response->getNextMaxId();
                } else {
                    $maxId = null;
                }

                if (isset($response->asArray()['comments'])) {
                    $items = $response->asArray()['comments'];
                    $comments = array_merge($comments,$items);
                }

            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
                dump($exception->getMessage());
                if ($exception->getMessage() == 'Throttled by Instagram because of too many API requests.') {
                    sleep(rand(7,12));
                } else {
                    throw $exception;
                }
            }

        } while ($maxId !== null);

        return $comments;
    }

    public function login($login, $passw)
    {
        try {
            if ($this->account->proxy == 1) {
                /**
                 * @var Proxy $proxy
                 */
                $proxy = Proxy::where('status', Proxy::STATUS_FREE)->first();
                if (isset($proxy)) {
                    $this->ig->setProxy($proxy->ip);
                }
                else {
                    /**
                     * @var Proxy $proxy
                     */
                    $proxy = Proxy::where('status', Proxy::STATUS_ON_PROCESS)->first();
                    $this->ig->setProxy($proxy->ip);
                }
            }

            $this->ig->login($login, $passw);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            if ($exception->getMessage() == 'User not logged in. Please call login() and then try again.') {
                throw $exception;
            }
        }
    }

    public function getFolowers($username, int $maxFollowers)
    {
        $rankToken = \InstagramAPI\Signatures::generateUUID();
        $userId = $this->getIdByUser($username);

        $followings = [];

        // Starting at "null" means starting at the first page.
        $maxId = null;

        do {

            if (count($followings) >= $maxFollowers) break;

            try {
                // Request the page corresponding to maxId.
                $response = $this->ig->people->getFollowing($userId, $rankToken, null, $maxId);

                $maxId = $response->getNextMaxId();

                $users = $response->asArray()['users'];

                $followings = array_merge($followings,$users);
            } catch (\Exception $exception) {
                if ($exception->getMessage() == 'Throttled by Instagram because of too many API requests.') {
                    sleep(rand(7,12));
                } else {
                    throw $exception;
                }
            }

        } while ($maxId !== null);

        if (count($followings) > $maxFollowers) {
            $followings = array_slice($followings,0, $maxFollowers);
        }

        return $followings;
    }

    public function getIdByUser($name)
    {
        return $this->ig->people->getUserIdForName($name);
    }

    public function getInfo($name)
    {
        return $this->ig->people->getInfoByName($name)->getUser();
    }

    public function getUserPosts($name)
    {
        $userId = $this->getIdByUser($name);

        return $this->ig->timeline->getUserFeed($userId);
    }

}