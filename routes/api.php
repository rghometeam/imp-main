<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/start', 'InstagramWorkerController@start')->name('start');
Route::get('/stop', 'InstagramWorkerController@stop')->name('stop');
Route::post('/import', 'InstagramWorkerController@import')->name('import');
Route::post('/login', 'InstagramWorkerController@addAccount')->name('addAccount');
Route::post('/send-code', 'InstagramWorkerController@finishVerification')->name('finishVerification');
Route::get('/export', 'InstagramWorkerController@export')->name('export');
Route::get('/accounts', 'InstagramWorkerController@getAccounts')->name('getAccounts');
Route::post('/profile/add', 'InstagramWorkerController@addProfile')->name('addProfile');
Route::post('/college/add', 'InstagramWorkerController@addCollege')->name('addCollege');
Route::get('/reset/profiles', 'InstagramWorkerController@resetProfiles')->name('resetProfiles');

Route::get('/statistic', 'StatisticController@getStatistic')->name('getStatistic');
